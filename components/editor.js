import React, { Component } from 'react';
let editorStyle = {
	"title": {
		fontSize: '40px'
	},
	"content": {
		fontSize: '20px',
		height: '500px'
	}
};
class Editor extends Component {
	constructor(props) {
		super(props);
		this.state = {
			content: '',
			title: ''
		};
	}
	handleButtonClick(event, name) {
		let currentTarget = event.currentTarget;
		document.execCommand(name, true, null);
		this.toggleActiveClass(currentTarget);
	}
	handleQuoteClick(event) {
		let currentTarget = event.currentTarget;
		document.execCommand('formatBlock', false, '<blockquote>');
		this.toggleActiveClass(currentTarget);
	}
	handleDropdown(event) {
		document.querySelector(".dropdown-menu").classList.toggle("show");
	}
	toggleActiveClass(currentTarget) {
		currentTarget.classList.toggle('active');
	}
	handleContentChange(event) {
		var value = event.target.value;
		this.setState({
			content: value
		});
	}
	handleTitleChange(event) {
		var value = event.target.value;
		this.setState({
			title: value
		});
	}
	render() {
		return (
			<div className="note-editor">
				<textarea className="post-title-field" placeholder="Title of your opinion"></textarea>
				<div className="note-toolbar btn-toolbar">
					<div className="note-font btn-group">
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+B" data-mac-shortcut="âŒ˜+B" data-event="bold" tabIndex="-1" data-original-title="Bold (âŒ˜+B)" onClick={(event) => this.handleButtonClick(event, "bold")}><i className="fa fa-bold icon-bold"></i>
						</button>
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+I" data-mac-shortcut="âŒ˜+I" data-event="italic" tabIndex="-1" data-original-title="Italic (âŒ˜+I)" onClick={(event) => this.handleButtonClick(event, "italic")}><i className="fa fa-italic icon-italic"></i>
						</button>
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+U" data-mac-shortcut="âŒ˜+U" data-event="underline" tabIndex="-1" data-original-title="Underline (âŒ˜+U)" onClick={(event) => this.handleButtonClick(event, "underline")}><i className="fa fa-underline icon-underline"></i>
						</button>
					</div>

					<div className="note-para btn-group">
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+8" data-mac-shortcut="âŒ˜+â‡§+7" data-event="insertUnorderedList" tabIndex="-1" data-original-title="Unordered list (âŒ˜+â‡§+7)" onClick={(event) => this.handleButtonClick(event, "insertUnorderedList")}><i className="fa fa-list-ul icon-list-ul"></i>
						</button>
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+7" data-mac-shortcut="âŒ˜+â‡§+8" data-event="insertOrderedList" tabIndex="-1" data-original-title="Ordered list (âŒ˜+â‡§+8)" onClick={(event) => this.handleButtonClick(event, "insertOrderedList")}><i className="fa fa-list-ol icon-list-ol"></i>
						</button>
						<button type="button" className="btn btn-default btn-sm btn-small dropdown-toggle" title="" data-toggle="dropdown" tabIndex="-1" data-original-title="Paragraph" onClick={(event)=>this.handleDropdown(event)}><i className="fa fa-align-left icon-align-left"></i> <span className="caret"></span>
						</button>
						<div className="dropdown-menu">
							<div className="note-align btn-group">
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+L" data-mac-shortcut="âŒ˜+â‡§+L" data-event="justifyLeft" tabIndex="-1" data-original-title="Align left (âŒ˜+â‡§+L)" onClick={(event) => this.handleButtonClick(event, "justifyLeft")}><i className="fa fa-align-left icon-align-left"></i>
								</button>
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+E" data-mac-shortcut="âŒ˜+â‡§+E" data-event="justifyCenter" tabIndex="-1" data-original-title="Align center (âŒ˜+â‡§+E)" onClick={(event) => this.handleButtonClick(event, "justifyCenter")}><i className="fa fa-align-center icon-align-center"></i>
								</button>
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+R" data-mac-shortcut="âŒ˜+â‡§+R" data-event="justifyRight" tabIndex="-1" data-original-title="Align right (âŒ˜+â‡§+R)" onClick={(event) => this.handleButtonClick(event, "justifyRight")}><i className="fa fa-align-right icon-align-right"></i>
								</button>
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+Shift+J" data-mac-shortcut="âŒ˜+â‡§+J" data-event="justifyFull" tabIndex="-1" data-original-title="Justify full (âŒ˜+â‡§+J)" onClick={(event) => this.handleButtonClick(event, "justifyFull")}><i className="fa fa-align-justify icon-align-justify"></i>
								</button>
							</div>
							<div className="note-list btn-group">
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+[" data-mac-shortcut="âŒ˜+[" data-event="outdent" tabIndex="-1" data-original-title="Outdent (âŒ˜+[)" onClick={(event) => this.handleButtonClick(event, "outdent")}><i className="fa fa-outdent icon-indent-left"></i>
								</button>
								<button type="button" className="btn btn-default btn-sm btn-small" title="" data-shortcut="Ctrl+]" data-mac-shortcut="âŒ˜+]" data-event="indent" tabIndex="-1" data-original-title="Indent (âŒ˜+])" onClick={(event) => this.handleButtonClick(event, "indent")}><i className="fa fa-indent icon-indent-right"></i>
								</button>
							</div>
						</div>
					</div>

					<div className="note-insert btn-group">
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-event="showLinkDialog" tabIndex="-1" data-original-title="Link"><i className="fa fa-link icon-link"></i>
						</button>
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-event="showImageDialog" tabIndex="-1" data-original-title="Picture"><i className="fa fa-picture-o icon-picture"></i>
						</button>
					</div>
					<div className="note-help btn-group">
						<button type="button" className="btn btn-default btn-sm btn-small" title="" data-event="quote" tabIndex="-1" data-original-title="quote" onClick={(event) => this.handleQuoteClick(event, "formatBlock")}>
							<i className="fa fa-quote-left icon-question"></i>
						</button>
					</div>
				</div>
				<div className="note-editable" contentEditable="true" spellCheck="false" onChange={(event)=>this.handleContentChange(event)} style={editorStyle["content"]} placeholder='Enter you content here'>
					{ this.state.content }
				</div>
			</div>

		);
	}
}
export default Editor;

