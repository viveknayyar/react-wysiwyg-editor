import React, { Component } from 'react';
import Editor from './editor';
import RightPanel from './right-panel';
class App extends Component {
	render() {
		return (
			<div>
				<div className="content-area write-box ">
					<div className="left-column write-area">
						<div className="creator-info">
							<img src="https://res.cloudinary.com/hashnode/image/upload/w_70,h_70,c_thumb,g_face/v1465161286/kltlrtni9qqxkyfmjhvm.jpg" className="user-photo" />
							<p className="user-name">Vivek Nayyar</p>
							<a className="toggle-preview " href="#">
								<i className="mdi mdi-eye"></i>
								<span className="preview-text">Preview</span>
							</a>
						</div>
						<Editor />
					</div>
					<RightPanel />
				</div>
			</div>
		);
	}
}
export default App;
