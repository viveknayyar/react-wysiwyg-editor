import React, { Component } from 'react';
export default class RightPanel extends Component {
	render() {
		return (
			<div className="right-column controls-area"><a className="open-controls" href="#"><i className="mdi mdi-chevron-left"></i></a>
				<div className="wrap-controls">
					<div className="single-control-block responsive-hide"><a className="publish-button " href="#"><i className="mdi mdi-send"></i>Ask Question</a>
					</div>
					<div className="single-control-block">
						<div className="post-tagging-wrapper">
							<p className="sub-heading"><i className="mdi mdi-tag-outline"></i>
								Tag Nodes
							</p>
							<div className="tags-search-input-wrapper">
								<input type="text" className="tags-input" placeholder="Start typing for suggestions.." />
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
